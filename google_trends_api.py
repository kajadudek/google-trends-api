from fastapi import FastAPI
from fastapi.responses import JSONResponse
import httpx
import json

app = FastAPI()

API_KEY = "vjg3K4HfPerioDm9k7UBLNQg"
TRENDS_URL = "https://www.searchapi.io/api/v1/search"
COUNTRY_URL = "https://restcountries.com/v3.1"

max_retries = 3
retry_wait = 5

from fastapi.responses import FileResponse


@app.get("/")
async def get_home():
    return FileResponse("index.html")


# get info about past *timeframe* trends
@app.get("/trends/")
async def get_trends(query: str, timeframe: str, geo: str):
    params = {
        "api_key": API_KEY,
        "data_type": "TIMESERIES",
        "engine": "google_trends",
        "q": query,
        "time": timeframe,
        "geo": geo,
        "tz": -60
    }

    try:
        async with httpx.AsyncClient(timeout=30.0) as client:
            response = await client.get(TRENDS_URL, params=params)

            # client side err
            if 400 <= response.status_code < 500:
                if response.status_code == 404:
                    return JSONResponse(status_code=404, content={"message": "Data not found."})
                return JSONResponse(status_code=response.status_code,
                                    content={"message": "Bad request. Please check the request parameters."})

            # server side err
            elif response.status_code >= 500:
                return JSONResponse(status_code=response.status_code, content={"message": "External service error"})

            elif response.status_code == 200:
                data = response.json()

                # parse data
                timeline_data = data.get('interest_over_time', {}).get('timeline_data', [])
                labels = [item['date'] for item in timeline_data]
                values = [item['values'][0]['extracted_value'] for item in timeline_data]

                # create chart URL
                chart_url = generate_quickchart_url((labels, values))
                return {"chart_url": chart_url}
    except httpx.ReadTimeout:
        return JSONResponse(status_code=408, content={"message": "Request timed out"})


@app.get("/countries")
async def get_countries_list():
    async with httpx.AsyncClient() as client:
        response = await client.get(COUNTRY_URL + "/all")

        if response.status_code == 200:
            data = response.json()

            names = [country["name"]["common"] for country in data]
            names.sort()

            return {"country_names": names}

        else:
            return handle_errors(response)


@app.get("/countries/code/{country_name}")
async def get_country_code(country_name: str):
    async with httpx.AsyncClient() as client:
        response = await client.get(COUNTRY_URL + "/name/" + f"{country_name}")

        if response.status_code == 200:
            data = response.json()

            country_code = data[0]["cca2"]
            return {"country_code": country_code}

        else:
            return handle_errors(response)


# helper functions
def chart_data_to_json(data):
    labels, values = data

    chart_config = {
        "type": "bar",
        "data": {
            "labels": labels,
            "datasets": [{"label": "Trend", "data": values}]
        }
    }

    return json.dumps(chart_config)


def generate_quickchart_url(data):
    chart_data = chart_data_to_json(data)
    chart_url = f"https://quickchart.io/chart?c={chart_data}&backgroundColor=white&width=500&height=300&devicePixelRatio=1.0&format=png&version=2.9.3"
    return chart_url


def handle_errors(response):
    # client side err
    if 400 <= response.status_code < 500:
        if response.status_code == 404:
            return JSONResponse(status_code=404, content={"message": "Data not found."})
        return JSONResponse(status_code=response.status_code,
                            content={"message": "Bad request. Please check the request parameters."})

    # server side err
    elif response.status_code >= 500:
        return JSONResponse(status_code=response.status_code, content={"message": "External service error"})

    return JSONResponse(status_code=response.status_code, content={"message": "An unexpected error."})

